.include "m168def.inc" 

.def delay1	= r17
.def delay2	= r18
.def delayv	= r19
.equ led	= 7 ; PORTD bit number to blink LED on

rjmp main

delay:
    clr delay1
    clr delay2
    ldi delayv, 100

delay_loop:
    dec   delay2
    brne  delay_loop
    dec   delay1
    brne  delay_loop
    dec   delayv
    brne  delay_loop
    ret              ; go back to where we came from


main:
     sbi PORTC, led
;     sbi 0x08, led
     sbi DDRC,  led       ; connect PORTD pin 4 to LED
;     sbi 0x07, led       ; connect PORTD pin 4 to LED

loop:
    sbi   PORTC, led      ; turn PD4 high
    rcall       delay   ; delay for an short bit
    cbi   PORTC, led      ; turn PD4 low 
    rcall       delay   ; delay again for a short bit
    rjmp        loop
